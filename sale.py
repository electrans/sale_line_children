# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, If, Bool, Or
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError
from decimal import Decimal

__all__ = ['Sale', 'SaleLine']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    lines_without_parent = fields.One2Many(
        'sale.line', 'sale', 'Lines',
        states={
            'readonly': Eval('state') != 'draft'},
        filter=[('parent', '=', None)],
        depends=['state'])

    def create_invoice(self):
        lines = self.lines
        self.lines = self.lines_without_parent
        invoices = super(Sale, self).create_invoice()
        for line in lines:
            if line not in self.lines:
                self.lines += (line,)
        self.save()
        return invoices

    @classmethod
    def copy(cls, sales, default=None):
        # set the lines_without_parent to None because if not, the lines get duplicated
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['lines_without_parent'] = None
        return super(Sale, cls).copy(sales, default=default)

    def check_for_quotation(self):
        if not self.warehouse:
            raise UserError(gettext('sale.msg_sale_warehouse_required_for_quotation', sale=self.rec_name))
        super(Sale, self).check_for_quotation()

    @classmethod
    def recompute_price_by_price_list(cls, sales, price_list):
        # Inherit method to ignore lines with parent too, the others methods to recompute is not necessary
        # cause unit_price is zero so their computing will result in zero what is the same
        pool = Pool()
        SaleLine = pool.get('sale.line')
        to_write = []
        cls.write(sales, {'price_list': price_list.id if price_list else None})
        for sale in sales:
            for line in sale.lines:
                if line.type != 'line' or line.parent:
                    continue
                new_values = sale._recompute_price_list_price(line)
                if new_values:
                        to_write.extend(([line], new_values))
        if to_write:
            SaleLine.write(*to_write)

    def search_lines_not_validated_terms(self):
        # Ignore lines that have parent filled
        validated_line_terms = super(Sale, self).search_lines_not_validated_terms()
        return [line for line in validated_line_terms if not line.parent]


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    parent = fields.Many2One(
        'sale.line', "Parent")
    children = fields.One2Many(
        'sale.line', 'parent', "Related products",
        states={
            'readonly': ~Eval('sale_state').in_(['draft', 'confirmed'])},
        context={
            'children': True},
        domain=[
            ('sale', '=', Eval('sale'))],
        depends=['sale'])
    position_sequence = fields.Function(
        fields.Char("Position"),
        'get_position')

    @classmethod
    def __setup__(cls):
        state = If(Eval('context', {}).contains('children'),
                   ~Eval('sale_state').in_(['draft', 'confirmed']),
                   Eval('sale_state') != 'draft')
        super(SaleLine, cls).__setup__()
        cls.product.states['readonly'] = ~Eval('sale_state').in_(['draft', 'confirmed'])
        cls.unit.states['readonly'] = state
        cls.quantity.states['readonly'] = state
        cls.product.domain = [
            If(Or(Bool(Eval('parent')), Bool(Eval('quotation_line'))),
                (),
                If(Eval('sale_state').in_(['draft']),
                    ('salable', '=', True),
                    ()))]
        cls.product.depends += ['parent', 'quotation_line']

    @fields.depends('product', 'sale', 'unit')
    def on_change_product(self):
        '''
        If self is a sale line child, do not set unit_price neither taxes.
        These will be at its parent line.
        '''
        if Transaction().context.get('children', False):
            if self.product:
                if self.product.sale_uom and self.product.sale_uom.category:
                    category = self.product.sale_uom.category
                    if not self.unit or self.unit.category != category:
                        self.unit = self.product.sale_uom
                        self.unit_digits = self.product.sale_uom.digits
                        self.unit_price = Decimal(0)
                        self.gross_unit_price = Decimal(0)
            else:
                self.unit = None
                self.unit_digits = None
                self.description = ''
        else:
            if self.sale and self.sale.state != 'confirmed':
                super(SaleLine, self).on_change_product()
            else:
                if not self.unit and self.product and self.product.default_uom:
                    self.unit = self.product.default_uom.id

    def on_change_quantity(self):
        # If self is a sale line child, do not set unit_price
        if not Transaction().context.get('children', False):
            super(SaleLine, self).on_change_quantity()

    @fields.depends('children')
    def on_change_with_shipping_date(self, name=None):
        "Modify shipping_date to return only the effective date if all moves are done"
        if self.children:
            dates = [child.shipping_date for child in self.children if child.shipping_date]
            shipping_date = min(dates) if dates else ''
        else:
            shipping_date = super(SaleLine, self).on_change_with_shipping_date(name)
        return shipping_date

    def get_position(self, name=None):
        "Do not return the position if the sale line have parent because it will be the same"
        return "" if self.parent else self.position or str(self.sequence)

    def get_moves(self):
        moves = []
        if self.children:
            for child in self.children:
                # check if the sale child have the moves attribute because if it's being created, it do not have it yet
                if getattr(child, 'moves', None):
                    moves.extend(child.moves)
            return moves
        return super(SaleLine, self).get_moves()

    def get_quantity(self):
        if self.children:
            return sum(children.quantity for children in self.children)
        return super(SaleLine, self).get_quantity()

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        new_lines = []
        for line in lines:
            if not line.parent:
                new_line, = super(SaleLine, cls).copy([line], default)
                new_lines.append(new_line)
                if line.children:
                    new_default = default.copy()
                    new_default['parent'] = new_line.id
                    new_default['sale'] = new_line.sale
                    # Create children of the current line linked with the new line and sale
                    for child in line.children:
                        new_line, = super(SaleLine, cls).copy([child], new_default)
                        new_lines.append(new_line)
        return new_lines


class SaleLineLeadTimesStart(metaclass=PoolMeta):
    __name__ = 'sale.line.lead.times.start'

    @classmethod
    def __setup__(cls):
        super(SaleLineLeadTimesStart, cls).__setup__()
        cls.lines.domain += [('parent', '=', None)]


class SaleImportLineStart(metaclass=PoolMeta):
    __name__ = 'sale.import_line.start'

    @classmethod
    def __setup__(cls):
        super(SaleImportLineStart, cls).__setup__()
        cls.lines.domain += [('parent', '=', None)]


class SaleLinePercentageStart(metaclass=PoolMeta):
    __name__ = 'sale.line.percentage.start'

    @classmethod
    def __setup__(cls):
        super(SaleLinePercentageStart, cls).__setup__()
        cls.lines.domain += [('parent', '=', None)]


class SaleLineMoveStart(metaclass=PoolMeta):
    __name__ = 'sale.line_move.start'

    @classmethod
    def __setup__(cls):
        super(SaleLineMoveStart, cls).__setup__()
        cls.lines_to_move.domain += [('parent', '=', None)]
        cls.line_position.domain += [('parent', '=', None)]
