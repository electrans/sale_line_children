# This file is part product_purchase_line_relation module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_electrans_sale_line_children import suite

__all__ = ['suite']
